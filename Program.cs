﻿using System;

namespace task18
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            Console.WriteLine("Task a");
            var a1 = 6;
            Console.WriteLine($"the value of num1 = {a1}");
            var a2 = 9;
            Console.WriteLine($"The value of num2 = {a2}");
            var a3 = a1+a2;
            Console.WriteLine($"The sum of the two is {a1+a2}");
        }
    }
}
